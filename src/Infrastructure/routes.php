<?php declare(strict_types = 1);

namespace App\Infrastructure;

use Generator;

/**
 * @return Generator<string,string>
 */
function routes(): Generator
{
    yield '/join' => 'Account\\Create';
    yield '/login' => 'Account\\Login';
    yield '/logout' => 'Account\\Logout';
    yield '/password' => 'Account\\Password';
    yield '/reset-password' => 'Account\\ResetPassword';
    yield '/server/{format:date|time}' => 'Server\\DateTime';
    yield '/server/version' => 'Server\\Version';
}
