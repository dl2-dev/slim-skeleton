<?php declare(strict_types = 1);

namespace App;

\define('App\\ENVIRONMENT', \getenv('APP_ENV') ?: 'development');

const VERSION = '1.0.0';
