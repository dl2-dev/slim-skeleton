<?php declare(strict_types = 1);

namespace App\Actions\Account;

use App\Actions\Controller;
use App\Exception\MissingFieldException;
use Slim\Http\Request;
use Slim\Http\Response;

class Create extends Controller
{
    protected const REQUIRES_AUTH = false;

    /**
     * ### Parameters
     *  - email: string.
     *  - password?: string.
     *  - type?: string.
     *  - username: string.
     *
     * ### Response
     */
    public function post(Request $req, Response $res, array $args): Response
    {
        /** @var string */
        $email = $req->getParsedBodyParam('email');

        /** @var string */
        $username = $req->getParsedBodyParam('username');

        if (!$email || !$username) {
            throw new MissingFieldException(['email', 'username']);
        }

        /** @var ?string */
        $password = $req->getParsedBodyParam('password');

        /** @var string */
        $type = $req->getParsedBodyParam('type', 'user');

        // @todo: create the user record for real
        return $res->withStatus(201)->withJson([
            'email'    => $email,
            'type'     => $type,
            'username' => $username,
        ]);
    }
}
