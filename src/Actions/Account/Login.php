<?php declare(strict_types = 1);

namespace App\Actions\Account;

use App\Actions\Controller;
use App\Exception\MissingFieldException;
use DL2\Slim\Utils\JWT;
use Slim\Http\Request;
use Slim\Http\Response;

class Login extends Controller
{
    protected const REQUIRES_AUTH = false;

    /**
     * ### Parameters
     *  - password: string.
     *  - type?: string.
     *  - username: string.
     *
     * ### Response
     */
    public function post(Request $req, Response $res): Response
    {
        /** @var string */
        $username = $req->getParsedBodyParam('username');

        if (!$username) {
            throw new MissingFieldException(['username']);
        }

        /** @var string */
        $password = $req->getParsedBodyParam('password');

        /** @var string */
        $type = $req->getParsedBodyParam('type', 'user');

        // @todo: authenticate the user for real
        $token = JWT::encode([
            'type'     => $type,
            'username' => $username,
        ]);

        return $res->withJson([
            'token' => $token,
        ]);
    }
}
