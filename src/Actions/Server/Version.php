<?php declare(strict_types = 1);

namespace App\Actions\Server;

use App\Actions\Controller;
use const App\VERSION;
use Slim\Http\Request;
use Slim\Http\Response;

class Version extends Controller
{
    protected const REQUIRES_AUTH = false;

    public function get(Request $req, Response $res, array $args): Response
    {
        return $res->withJson(VERSION);
    }
}
