<?php declare(strict_types = 1);

namespace App\Actions\Server;

use App\Actions\Controller;
use Slim\Http\Request;
use Slim\Http\Response;

class DateTime extends Controller
{
    protected const REQUIRES_AUTH = false;

    /**
     * @param array{format:'date'|'time'} $args
     */
    public function get(Request $req, Response $res, array $args): Response
    {
        if ('time' === $args['format']) {
            return $res->withJson(\time());
        }

        return $res->withJson(\date('c'));
    }
}
