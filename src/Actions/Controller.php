<?php declare(strict_types = 1);

namespace App\Actions;

use DL2\Slim;

abstract class Controller extends Slim\Controller
{
    protected function init(): void
    {
        // intentionally left blank
    }
}
