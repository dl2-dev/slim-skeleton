module.exports = {
  extends: ["@commitlint/config-conventional"],

  // prettier-ignore
  rules: {
    "scope-enum": [
      2,
      "always",
      [
        "action",
        "assets",
        "build",
        "ci",
        "deploy",
        "domain",
        "infrastructure",
        "middleware",
        "tests",
      ],
    ],
  }
};
